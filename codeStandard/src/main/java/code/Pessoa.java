package code;

public class Pessoa {
    private int idadeDaPessoa;
    private String nomeDaPessoa;

    public String getNomeDaPessoa() {
        return nomeDaPessoa;
    }

    public void setNomeDaPessoa(String nomeDaPessoa) {
        this.nomeDaPessoa = nomeDaPessoa;
    }

    public int getIdadeDaPessoa() {
        return idadeDaPessoa;
    }

    public void setIdadeDaPessoaAqui(int idadeDaPessoa) {
        this.idadeDaPessoa = idadeDaPessoa;
    }
}
